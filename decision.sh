
#!/bin/bash
# it serves as code quality analyser to decide
# whether code quality is low or high
# it publishes then the decision in an HTMl file

# complexity code report file
file='public/result.txt'
# severity minor counter
count_min=0
# severity major counter
count_maj=0
minor="\"minor\""
major="\"major\""

# read file content line by line
while read line; do
   echo "$line"

   if    [[ $line == "$minor" ]];
           then
             ((count_min++))
   elif [[ $line == "$major" ]];
           then
             ((count_maj++))
   fi
done < $file

# now check whether code quality is high or low
# and store the decision result in an html file
echo "Total minor severity is:" $count_min
echo "Total major severity is:" $count_maj
echo "start the analysis"
echo "<!DOCTYPE html>" > public/decision.html
echo "<html lang="en">" >>  public/decision.html
echo "<body>" >>  public/decision.html
if [[  $count_maj  -lt $count_min ]];
   then
     echo "<p style=\"color:green;\"> code quality is High </p>"  >> public/decision.html
else
     echo "<p style=\"color:red;\"> code quality is low </p>"  >> public/decision.html
fi
echo "<body>" >>  public/decision.html
echo "</html>" >>  public/decision.html
echo " Decision taken and the code quality is published"
